#################
output "enable" {
  ###############
  value       = var.enable
  description = "Path-through value of var.enable"
}

##############
output "env" {
  ############
  value       = var.env
  description = "Path-through value of var.env"
}

###################
output "instance" {
  #################
  value       = try(aws_instance.this[0], aws_spot_instance_request.this[0], null)
  description = "aws_instance or aws_spot_instance_request object"
}

###################
output "iam_role" {
  #################
  value       = try(aws_iam_role.this[0], null)
  description = "aws_iam_role object"
}

###################
output "key_pair" {
  #################
  value       = try(aws_key_pair.this[0], null)
  description = "aws_key_pair object"
}
